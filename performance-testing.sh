#!/bin/bash

## This script downloads the sample project.
## It also downloads the PerfCI toolchain, adds performance testing tasks and pushes all the necessary CI files.


## Checking for the necessary packages

# Function to check commands
function checkCommand(){
        _=$(command -v $1);
        if [ "$?" != "0" ]; then
                echo $?
                printf -- '\033[31m ERROR: You dont seem to have %s installed \033[0m\n';
                exit 127;
        fi;
}


printf -- '\033[31m -------------------------------- \033[0m\n' ;
printf -- '\033[31m Starting PERFCI Toolchain Runner \033[0m\n' ;
printf -- '\033[31m -------------------------------- \033[0m\n' ;

# First check whether the necessary package are installed
checkCommand virtualenv
checkCommand git
checkCommand curl

## Setting up virtual environment
echo "Setting up virtual environment"
virtualenv perfci-venv --python=python2.7
source perfci-venv/bin/activate


## Clone the sample project

echo "Pulling the sample project"
git clone https://package4conf:abcd1234@gitlab.com/package4conf/sample-perfci.git

cd sample-perfci

## Clone our perfci toolchain

echo "Pulling PERFCI Toolchain"
git clone https://gitlab.cern.ch/omjaved/perfci

cd perfci

git checkout 2fde7f25d7915af73efe705683e4c4658e099e87

pip install -r requirements.txt

# Copy necessary files from the sample directory in perfci to the sample projects
cp -rf sample/perfdata ../
cp sample/.perf-ci.yml ../
cp sample/perfdata/VyPR_queries.py ../
cp sample/.gitlab-ci.yml ../

cd ..

echo "Running the Toolchain"
# Run step 1 of the toolchain
python perfci/ymlparser.py 


echo "Pushing commits to initiate CI pipeline with PERFCI"
## Commit all the changes
git add perfdata/* .gitlab-ci.yml

git commit -m 'running performance testing'

git push

## Bring the repository back to its initialize state.
echo "Reset the repository to its initial state"
git rm -rf perfdata
git rm .gitlab-ci.yml

git commit -m 'cleanup'
git push


printf -- '\033[31m Go to https://gitlab.com/package4conf/sample-perfci/pipelines to see the pipeline status \033[0m\n';
