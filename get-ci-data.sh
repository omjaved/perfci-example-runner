#!/bin/bash


## This scripts downloads CI data from the project.

printf -- '\033[31m Starting Analysis library  \033[0m\n';

## Checking for the necessary packages
# Function to check commands

# We first create a data directory to keep our results
mkdir data

# Download verdict database from gitlab artifact
curl -L -O "https://gitlab.com/package4conf/sample-perfci/-/jobs/artifacts/master/raw/perfdata/verdicts.db?job=performance_testing"
mv verdicts.db?job=performance_testing data/verdicts.db

# Download VyPR Analysis library and its dependencies
git clone https://github.com/pyvypr/VyPR.git
cd VyPR 
git checkout 4f6461adfe14ca5ffcebcf5237122c781ed067e1
cd ..

git clone https://github.com/pyvypr/VyPRServer.git
cp -rf VyPR VyPRServer

cd VyPRServer
git checkout 8ef27c2db0ff234800c520e8741ca931dbfdf3a8
cd ..

git clone https://github.com/pyvypr/VyPRAnalysis.git
cd VyPRAnalysis
git checkout ad6c4e043772d35cda6bd8e641bd5f43ac5e2dba
cd ..


# Run our analysis script
python unit_test_analysis.py


# Cleanup files
rm -rf data log* VyPR* perfci-venv sample-perfci

printf -- '\033[31m The pipeline is completed  \033[0m\n';