### This repository contains the scripts for automating PerfCI toolchain.

:exclamation: IMPORTANT: For Tool demonstration reviewers, please run the pipeline with your anonymous git username i.e., any username that does not let authors of the tool know about reviewers


## Automating performance testing tasks and monitoring with extended VyPR for CI

These steps are automated by `performance-testing.sh` script

## Visualizing data with CI extended VyPR Analysis Library

This step is performed after the CI pipeline is completed successfully. This is accomplished by the `get-ci-data.sh`script.

## Prerequisite

We will need `curl`, `virtualenv` and `git` installed in order for the scripts to work

 
## Instructions

Run `performance-testing.sh` script

This will execute all the necessary steps for performance testing and create a pipeline in CI.

Observe when the pipeline completes successfully by visiting the following link

`https://gitlab.com/package4conf/sample-perfci/pipelines`

It will take few minutes to complete the pipeline.

Run `get-ci-data.sh` script.

This will analyze the data via analysis library.

## Specification being monitored

We monitor for a bottleneck in a function 'check' which should not take more than 5 seconds to execute.

```
from VyPR.QueryBuilding import *
verification_conf = \
{
     "src.main" : {
        "add" : [
            Forall(
                t = calls('check')
            ).Check(
                lambda t : (
                    t.duration()._in([0, 5])
                )
            )
        ]

    }
}

```


## Expected output

A tabular display with two columns Measured value and Verdict. Since the contraint is placed between 0 and 5. Therefore, any measured value above 5 seconds will have a verdict value of 'Specification violation'


